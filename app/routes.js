/**
 * Name:  kelvin Isaiah Ayonga
 * Email: kelvin6600@gmail.com
 *
 * @ngdoc ui.route
 * @name visitor.management
 * @description
 * The main module for the visitor management app
 */

(function(){

    'use strict';

    angular.module('AddressBook')
        .config([ "$stateProvider", "$urlRouterProvider", routeConfig ]);

    /**
     * This function contains the route configurations
     * for the application
     *
     * @param $stateProvider
     * @param $urlRouterProvider
     */
    function routeConfig($stateProvider,$urlRouterProvider){
        /**
         * app router for global app routing
         */
        $urlRouterProvider.otherwise('/');
        $stateProvider.state('home', {
            url: '/',
            templateUrl: 'app/home.tpl.html',
            //controller: 'globalAppCtrl as ext',
            data: {
                requiresLogin: true
            }
        });
    }
})();