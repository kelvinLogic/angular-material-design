/**
 * Created by kelvin on 8/15/2015.
 */

module.exports = function(grunt){
    grunt.initConfig({

        /**
         * Task for processing javascript files
         */
        bowerInstall: {

            target: {

                // Point to the files that should be updated when
                // you run `grunt bower-install`
                src: [
                    'index.html'   // .html support...
                ]            }
        }
    }); //initConfig Section

    grunt.loadNpmTasks('grunt-bower-install');

    grunt.registerTask('default',['bowerInstall']);

}; //wrapper function